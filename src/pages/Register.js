import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react"; //to store value from inputfields
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

// form handling
export default function Register(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password1, setPassword1] = useState("");
  const [isActive, setIsActive] = useState(false);

  const { user, setUser } = useContext(UserContext);

  useEffect(() => {
    if (
      email !== "" &&
      password !== "" &&
      password1 !== "" &&
      password === password1
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password1]);

  function registerUser(e) {
    e.preventDefault();

    localStorage.setItem("email", email);

    setUser({
      email: localStorage.getItem("email"),
    });

    setEmail("");
    setPassword("");
    setPassword1("");
    alert("Thank you for registering " + email);
  }

  return user.email !== null ? (
    <Navigate to="/courses/" />
  ) : (
    <div>
      <h1 className="mt-4">Register Here</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label className="mt-2">Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Please input your email here."
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted">
            {" "}
            We'll never share your email with anyone else. 😏
          </Form.Text>
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label className="mt-2">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Please input your password here."
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password2">
          <Form.Label className="mt-2">Verify Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Please verify your password here."
            required
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
          />
        </Form.Group>
        {isActive ? (
          <Button
            variant="success"
            type="submit"
            id="submitBTN"
            className="mt-3 mb-5"
          >
            Register
          </Button>
        ) : (
          <Button
            variant="danger"
            type="submit"
            id="submitBTN"
            className="mt-3 mb-5"
            disabled
          >
            Register
          </Button>
        )}
      </Form>
    </div>
  );
}
