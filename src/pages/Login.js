import { Form, Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  //   const loginSuccess = () => {
  //     return alert(email + " has been verified. Welcome back!");
  //   };

  function logInUser(e) {
    e.preventDefault();

    localStorage.setItem("email", email);

    setUser({
      email: localStorage.getItem("email"),
    });

    setEmail("");
    setPassword("");
    alert(email + " has been verified. Welcome back!");
  }
  return user.email !== null ? (
    <Navigate to="/courses/" />
  ) : (
    <div>
      <h1 className="mt-4">Login Here</h1>
      <Form onSubmit={(e) => logInUser(e)}>
        <Form.Group controlId="userEmail1">
          <Form.Label className="mt-2">Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password1">
          <Form.Label className="mt-2">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {isActive ? (
          <Button
            variant="success"
            type="submit"
            id="submitBTN1"
            className="mt-3 mb-5"
            // onClick={loginSuccess}
          >
            Register
          </Button>
        ) : (
          <Button
            variant="danger"
            type="submit"
            id="submitBTN1"
            className="mt-3 mb-5"
            disabled
          >
            Register
          </Button>
        )}
      </Form>
    </div>
  );
}
