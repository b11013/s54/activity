import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AppNavBar from "./components/AppNavBar";
import "./App.css"; //from React
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import ErrorPage from "./pages/ErrorPage";
import { useState } from "react";
import { UserProvider } from "./UserContext";

function App() {
  //for logged in
  const [user, setUser] = useState({
    email: localStorage.getItem("email"),
  });

  //for logged out - put provider
  const unsetUser = () => {
    localStorage.clear();
  };

  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<ErrorPage />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

// if its from react u need closing pair tags
//if its from component it is self closing.
